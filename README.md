# Kubernetes Terraform

## Setup

1. Import the `kube-system` namespace
  * `terraform import module.namespaces.module.kube_system.kubernetes_namespace.namespace kube-system`
1. Import the `default` namespace
  * `terraform import module.namespaces.module.default.kubernetes_namespace.namespace default`

## Execute

1. Run `terraform plan -out=plan.file`
1. Check the output and make sure everything looks correct
1. Run `terraform apply plan.file`
