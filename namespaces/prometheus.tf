module "prometheus" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "prometheus"
}

resource "kubernetes_service_account" "prometheus" {
  metadata {
    name      = "prometheus"
    namespace = "${module.prometheus.namespace}"
  }
}

resource "kubernetes_cluster_role" "prometheus" {
  metadata {
    name = "prometheus"
  }

  rule {
    api_groups = [""]

    resources = [
      "namespaces",
      "nodes",
      "persistentvolumeclaims",
      "pods",
      "services",
      "resourcequotas",
      "replicationcontrollers",
      "limitranges",
      "persistentvolumeclaims",
      "persistentvolumes",
      "endpoints",
    ]

    verbs = [
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = ["extensions"]

    resources = [
      "daemonsets",
      "deployments",
      "replicasets",
    ]

    verbs = [
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = ["apps"]
    resources  = ["statefulsets"]

    verbs = [
      "get",
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = ["batch"]

    resources = [
      "cronjobs",
      "jobs",
    ]

    verbs = [
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = ["autoscaling"]
    resources  = ["horizontalpodautoscalers"]

    verbs = [
      "list",
      "watch",
    ]
  }
}

resource "kubernetes_cluster_role_binding" "prometheus" {
  metadata {
    name = "prometheus"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "${lookup(kubernetes_cluster_role.prometheus.metadata[0], "name")}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.prometheus.metadata[0], "name")}"
    namespace = "${module.prometheus.namespace}"
  }
}
