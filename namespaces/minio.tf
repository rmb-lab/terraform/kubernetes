module "minio" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "minio"
}

resource "kubernetes_service_account" "minio-vault" {
  metadata {
    name      = "minio-vault"
    namespace = "${module.minio.namespace}"
  }
}

resource "kubernetes_service_account" "minio-b2" {
  metadata {
    name      = "minio-b2"
    namespace = "${module.minio.namespace}"
  }
}

resource "kubernetes_service_account" "minio-sync" {
  metadata {
    name      = "minio-sync"
    namespace = "${module.minio.namespace}"
  }
}
