module "gitlab" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "gitlab"
}

resource "kubernetes_service_account" "gitlab" {
  metadata {
    name      = "gitlab"
    namespace = "${module.gitlab.namespace}"
  }
}

resource "kubernetes_role_binding" "gitlab" {
  metadata {
    name      = "gitlab-jobs"
    namespace = "${module.gitlab_jobs.namespace}"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "edit"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.gitlab.metadata[0], "name")}"
    namespace = "${module.gitlab.namespace}"
  }
}

module "gitlab_jobs" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "gitlab-jobs"
}

resource "kubernetes_service_account" "gitlab-job-terraform-vault" {
  metadata {
    name      = "gitlab-job-terraform-vault"
    namespace = "${module.gitlab_jobs.namespace}"
  }
}
