module "ingress" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "ingress"
}

resource "kubernetes_service_account" "ingress" {
  metadata {
    name      = "ingress"
    namespace = "${module.ingress.namespace}"
  }
}

resource "kubernetes_role" "ingress" {
  metadata {
    name      = "ingress"
    namespace = "${module.ingress.namespace}"
  }

  rule {
    api_groups = [""]

    resources = [
      "configmaps",
      "pods",
      "secrets",
      "namespaces",
    ]

    verbs = [
      "get",
    ]
  }

  rule {
    api_groups     = [""]
    resources      = ["configmaps"]
    resource_names = ["ingress-controller-leader-nginx"]

    verbs = [
      "get",
      "update",
    ]
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps"]
    verbs      = ["create"]
  }

  rule {
    api_groups = [""]
    resources  = ["endpoints"]

    verbs = [
      "get",
      "create",
      "update",
    ]
  }
}

resource "kubernetes_cluster_role" "ingress" {
  metadata {
    name = "ingress"
  }

  rule {
    api_groups = [""]

    resources = [
      "configmaps",
      "endpoints",
      "nodes",
      "pods",
      "secrets",
    ]

    verbs = [
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["get"]
  }

  rule {
    api_groups = [""]
    resources  = ["services"]

    verbs = [
      "get",
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = [""]
    resources  = ["events"]

    verbs = [
      "create",
      "patch",
    ]
  }

  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses"]

    verbs = [
      "get",
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses/status"]

    verbs = [
      "update",
    ]
  }
}

resource "kubernetes_role_binding" "ingress" {
  metadata {
    name      = "ingress"
    namespace = "${module.ingress.namespace}"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "${lookup(kubernetes_role.ingress.metadata[0], "name")}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.ingress.metadata[0], "name")}"
    namespace = "${module.ingress.namespace}"
  }
}

resource "kubernetes_cluster_role_binding" "ingress" {
  metadata {
    name = "ingress"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "${lookup(kubernetes_cluster_role.ingress.metadata[0], "name")}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.ingress.metadata[0], "name")}"
    namespace = "${module.ingress.namespace}"
  }
}
