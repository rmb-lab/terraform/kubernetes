module "vault" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "vault"
}

resource "kubernetes_service_account" "vault_k8s_auth" {
  metadata {
    name      = "vault-k8s-auth"
    namespace = "${module.vault.namespace}"
  }
}

resource "kubernetes_cluster_role_binding" "vault_k8s_auth" {
  metadata {
    name = "vault-k8s-auth"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "system:auth-delegator"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.vault_k8s_auth.metadata[0], "name")}"
    namespace = "${module.vault.namespace}"
  }
}
