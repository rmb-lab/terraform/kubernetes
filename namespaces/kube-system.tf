module "kube_system" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "kube-system"

  limits = {
    cpu    = "250m"
    memory = "256Mi"
  }
}

resource "kubernetes_service_account" "terraform" {
  metadata {
    name      = "terraform"
    namespace = "${module.kube_system.namespace}"
  }
}

resource "kubernetes_cluster_role" "terraform" {
  metadata {
    name = "terraform"
  }

  rule {
    api_groups = [""]

    resources = [
      "namespace",
      "serviceaccount",
    ]

    verbs = ["*"]
  }

  rule {
    api_groups = ["rbac.authorization.k8s.io"]

    resources = [
      "clusterrole",
      "role",
      "clusterrolebinding",
      "rolebinding",
    ]

    verbs = ["*"]
  }
}

resource "kubernetes_cluster_role_binding" "terraform" {
  metadata {
    name = "terraform"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "${lookup(kubernetes_cluster_role.terraform.metadata[0], "name")}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.terraform.metadata[0], "name")}"
    namespace = "${module.kube_system.namespace}"
  }
}

resource "kubernetes_service_account" "kube_router" {
  metadata {
    name      = "kube-router"
    namespace = "${module.kube_system.namespace}"
  }
}

resource "kubernetes_cluster_role" "kube_router" {
  metadata {
    name = "kube-router"
  }

  rule {
    api_groups = [""]

    resources = [
      "namespaces",
      "pods",
      "services",
      "nodes",
      "endpoints",
    ]

    verbs = [
      "list",
      "get",
      "watch",
    ]
  }

  rule {
    api_groups = [
      "extensions",
    ]

    resources = ["networkpolicies"]

    verbs = [
      "list",
      "get",
      "watch",
    ]
  }

  rule {
    api_groups = [
      "networking.k8s.io",
    ]

    resources = ["networkpolicies"]

    verbs = [
      "list",
      "get",
      "watch",
    ]
  }
}

resource "kubernetes_cluster_role_binding" "kube_router" {
  metadata {
    name = "kube-router"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "${lookup(kubernetes_cluster_role.kube_router.metadata[0], "name")}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.kube_router.metadata[0], "name")}"
    namespace = "${module.kube_system.namespace}"
  }
}
