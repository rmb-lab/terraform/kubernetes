module "sandwich" {
  source = "git::https://gitlab.com/rmb-lab/terraform/modules.git?ref=0.1.16//namespace"
  name   = "sandwich"
}

resource "kubernetes_service_account" "sandwich" {
  metadata {
    name      = "sandwich"
    namespace = "${module.sandwich.namespace}"
  }
}

resource "kubernetes_cluster_role" "sandwich" {
  metadata {
    name = "sandwich"
  }

  rule {
    non_resource_urls = [
      "/version",
      "/version/",
    ]

    verbs = ["get"]
  }

  rule {
    api_groups = [""]

    resources = [
      "namespaces",
      "endpoints",
    ]

    verbs = ["*"]
  }

  rule {
    api_groups = ["sandwichcloud.com"]
    resources  = ["*"]
    verbs      = ["*"]
  }

  rule {
    api_groups = ["apiextensions.k8s.io"]
    resources  = ["customresourcedefinitions"]

    verbs = [
      "get",
      "list",
      "create",
    ]
  }
}

resource "kubernetes_cluster_role_binding" "sandwich" {
  metadata {
    name = "sandwich"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "${lookup(kubernetes_cluster_role.sandwich.metadata[0], "name")}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${lookup(kubernetes_service_account.sandwich.metadata[0], "name")}"
    namespace = "${module.sandwich.namespace}"
  }
}
