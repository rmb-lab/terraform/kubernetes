resource "kubernetes_storage_class" "vsphere" {
  metadata {
    name = "vsphere"
    annotations = {
      "storageclass.kubernetes.io/is-default-class" = "true"
    }
  }
  storage_provisioner = "kubernetes.io/vsphere-volume"
}
